const dotenv = require('dotenv');
dotenv.config();

console.log(process.env.PD_DB_HOST);
console.log(process.env.PD_DB_USER);
console.log(process.env.PD_DB_PASS);
console.log(process.env.PD_DB_DATABASE);


module.exports = {
    sequelize: {
        HOST: process.env.PD_DB_HOST || "localhost",
        USER: process.env.PD_DB_USER || "stefan",
        PASSWORD: process.env.PD_DB_PASS || "Mandeep@123",
        DB: process.env.PD_DB_DATABASE || "ipl__data",
        dialect: "mysql",

        pool: {
            max: 5,
            min: 0,
            acquire: 30000,
            idle: 10000
        }
    }
};