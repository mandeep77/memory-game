const express = require('express');
const path = require('path');
const { users } = require("./models/index");;
const nodemailer = require('nodemailer');
const dotenv = require('dotenv');
const mg = require('nodemailer-mailgun-transport')
dotenv.config();
const port = process.env.PORT || 7777;


const app = express();

app.use(express.static(path.join(__dirname, 'public')))


app.use(express.json())
app.use(express.urlencoded({ extended: false }));
const db = require("./models");



db.sequelize.sync();

// db.sequelize.sync({ force: true }).then(() => {
//     console.log("Drop and re-sync db.");
// });

let auth = {
    auth: {
        api_key: process.env.MAILGUN_API_KEY,
        domain: process.env.MAILGUN_DOMAIN
    }
}
const transporter = nodemailer.createTransport(mg(auth));
// create reusable transporter object using the default SMTP transport

// let transporter = nodemailer.createTransport({
//     host: "smtp.google.com",
//     port: 587,
//     secure: false, // true for 465, false for other ports
//     auth: {
//         user: 'memandeep7@gmail.com', // generated ethereal user
//         pass: 'gurunanak@123', // generated ethereal password
//     },
// });



app.get("/all", async(req, res) => {

    try {
        const users1 = await users.findAll({
            // where: conditions, 
            order: [
                ["score", "ASC"]
            ],
            limit: 5,
        });

        res.status(200).send(users1);
        res.end();

    } catch (error) {
        console.log(error);
    }



});
app.post('/api/users', async(req, res) => {

    try {
        let users1 = {
            user_name: req.body.name,
            score: req.body.score,
            email: req.body.email
        }
        let info = await transporter.sendMail({
            from: 'noreply@memory-game.com', // sender address
            to: req.body.email, // list of receivers
            subject: `Hello ${req.body.name}`, // Subject line
            text: `YOUR MEMORY GAME SCORE : ${req.body.score}`, // plain text body
            // html: "<b>`YOUR MEMORY GAME SCORE : ${req.body.score}`</b>", // html body
        });
        users.create(users1)
        res.redirect('/game.html');
        res.end();

    } catch (error) {
        console.log(error);
    }
})


app.get('/bestScore', async(req, res) => {

    try {
        const best = await users.findAll({
            // where: conditions, 
            order: [
                ["score", "ASC"]
            ],
            limit: 1,
        });

        res.status(200).send(best);
        res.end();
    } catch (error) {
        console.log(error);
    }

})
























app.listen(port, () => console.log(`Server started at port ${port}`))