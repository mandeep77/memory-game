const gameContainer = document.getElementById("game");;
const audio = new Audio('./devil.mp3');


function openNav() {
    document.getElementById("mySidepanel").style.width = "700px";
}

/* Set the width of the sidebar to 0 (hide it) */
function closeNav() {
    document.getElementById("mySidepanel").style.width = "0";
}
//fetching the top 5 players from the database
const scorePath = '/all'
fetch(scorePath).then(res => res.json()).then(users)


//function to append the top5 players to the html file
function users(data) {
    data.forEach(element => {
        console.log(element)
        const divOfPlayers = document.getElementById('players')
        const div = document.createElement("div");
        const name = document.createElement("h4");
        const score = document.createElement("h3");
        const date = document.createElement("h2");
        name.innerText = element.user_name;
        score.innerText = element.score;
        date.innerText = element.createdAt.slice(0, 10)
        div.appendChild(name);
        div.appendChild(score);
        div.appendChild(date);
        divOfPlayers.appendChild(div);
    });


}


document.addEventListener("touchstart", function() {}, true)
const COLORS = [
    "gif11",
    "gif4",
    "gif1",
    "gif2",
    "gif10",
    "gif3",
    "gif5",
    "gif5",
    "gif10",
    "gif6",
    "gif7",
    "gif4",
    "gif12",
    "gif8",
    "gif3",
    "gif1",
    "gif8",
    "gif9",
    "gif11",
    "gif12",
    "gif2",
    "gif6",
    "gif7",
    "gif9",

];

// here is a helper function to shuffle an array
// it returns the same array with values shuffled
// it is based on an algorithm called Fisher Yates if you want ot research more
function shuffle(array) {
    let counter = array.length;

    // While there are elements in the array
    while (counter > 0) {
        // Pick a random index
        let index = Math.floor(Math.random() * counter);

        // Decrease counter by 1
        counter--;

        // And swap the last element with it
        let temp = array[counter];
        array[counter] = array[index];
        array[index] = temp;
    }

    return array;
}

let shuffledColors = shuffle(COLORS);

// this function loops over the array of colors
// it creates a new div and gives it a class with the value of the color
// it also adds an event listener for a click for each card
function createDivsForColors(colorArray) {
    for (let color of colorArray) {
        // create a new div
        const newDiv = document.createElement("div");

        // give it a class attribute for the value we are looping over


        newDiv.classList.add(color, 'colorless');
        setTimeout(() => {
            newDiv.classList.add('test');

        }, 1000);
        setTimeout(() => {
            newDiv.classList.remove('test');

        }, 2000);
        // newDiv.setAttribute("class", "gogo");
        // console.log(newDiv);

        // call a function handleCardClick when a div is clicked on
        newDiv.addEventListener("click", handleCardClick);

        // append the div to the element with an id of game
        gameContainer.append(newDiv);
    }
}



let counter = 0;
let matchCardCounter = 0;
let flip = false;
let first, second;
let freeze = false;
let arrayP = [];




function reset(e) {
    e.preventDefault();
    if (arrayP.length == 0) {
        const freezeScreen = document.getElementById('freezeScreen').classList;
        const warning = document.getElementById('warning').classList
        freezeScreen.remove('none');
        warning.remove('none');
        const ok = document.getElementById('continue');
        console.log('here')
        ok.addEventListener("click", (ee) => {
            ee.preventDefault()
            freezeScreen.add('none');
            warning.add('none');
        })

    } else {
        arrayP.forEach(div => {


            const freezeScreen = document.getElementById('freezeScreen').classList;
            const dialog = document.getElementById('dialog').classList

            freezeScreen.remove('none');
            dialog.remove('none');

            const confrom = document.getElementById('btn1');

            confrom.addEventListener('click', (eee) => {
                eee.preventDefault()
                div.classList.add('colorless');
                div.addEventListener("click", handleCardClick);
                document.getElementById('score').innerText = 0;
                freezeScreen.add('none');
                dialog.add('none');
                counter = 0;
                matchCardCounter = 0;
                flip = false;
                freeze = false;
                arrayP = [];
                first = null;
                second = null;
                document.getElementById('status').innerText = ''

            })

            const no = document.getElementById('btn2');
            no.addEventListener('click', (eeee) => {
                eeee.preventDefault();
                freezeScreen.add('none');
                dialog.add('none');

            })
        });
    }


}

//fetching best score from the database
const bestScoreTag = document.getElementById('hscore');
try {
    fetch('/bestScore').then(res => res.json()).then(bestScore);
} catch (error) {
    console.log(error);
}

//function to display the best score 
function bestScore(player) {
    try {
        player.forEach(user => {
            console.log(user.score)
            const score = user.score
            bestScoreTag.innerHTML = score
        })
    } catch (error) {
        console.log(error);
    }
}

function flipFunc(ele) {

    if (freeze == true || ele == first) {

        return;

    } else {
        if (flip !== true) {
            counter += 1;
            document.getElementById('score').innerText = counter;
            flip = true;

            ele.classList.remove('colorless');
            first = ele;
            // audio.play();

            arrayP.push(first);

        } else {

            flip = false;
            counter += 1;
            document.getElementById('score').innerText = counter;
            ele.classList.remove('colorless')
            second = ele;

            if (first.classList[0] == second.classList[0]) {

                matchCardCounter += 1;
                first.removeEventListener("click", handleCardClick);
                second.removeEventListener("click", handleCardClick);
                arrayP.push(first, second);
                console.log(arrayP)
                console.log(counter)

                if (matchCardCounter === 12) {
                    fetch(scorePath).then(res => res.json()).then(users)


                    const freezeScreen = document.getElementById('freezeScreen').classList;

                    const over = document.getElementById('over').classList
                        // module.exports = counter;

                    const scoredata1 = document.getElementById('scoredata1')
                    scoredata1.value = counter
                        // document.getElementById("scoredata").submit();

                    freezeScreen.remove('none');
                    over.remove('none');
                    const email = document.getElementById('email')
                    const emailValue = email.value;
                    console.log(emailValue)


                    const restartgame = document.getElementById('restartgame');
                    console.log(restartgame)

                    restartgame.addEventListener('click', () => {
                        arrayP.forEach(div => {
                            div.classList.add('colorless');
                            div.addEventListener("click", handleCardClick);
                            document.getElementById('score').innerText = 0;
                            freezeScreen.add('none');
                            over.add('none');
                            counter = 0;
                            matchCardCounter = 0;
                            flip = false;
                            freeze = false;
                            arrayP = [];
                            first = null;
                            second = null;
                            document.getElementById('status').innerText = ''
                        })
                    })
                }

            } else {

                freeze = true
                setTimeout(() => {
                    first.classList.add('colorless')
                    second.classList.add('colorless')
                        // arrayP = arrayP.map(element => element !== first || element !== second)
                    freeze = false;
                    first = null;
                    second = null;
                }, 1000);

            }
        }
    }


}


// TODO: Implement this function!
function handleCardClick(event) {
    // event.dblclick();
    flipFunc(event.target);


}

const button = document.getElementById('restart');
button.addEventListener('click', reset);



// when the DOM loads
createDivsForColors(shuffledColors);