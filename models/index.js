const connection = require("../config/connection.js");

const Sequelize = require("sequelize");
const sequelize = new Sequelize(connection.database, connection.user, connection.password, {
    host: connection.host,
    dialect: connection.dialect,

    pool: {
        max: connection.pool.max,
        min: connection.pool.min,
        acquire: connection.pool.acquire,
        idle: connection.pool.idle
    }
});

const db = {};

db.Sequelize = Sequelize;
db.sequelize = sequelize;

db.users = require("./highScore")(sequelize, Sequelize);


module.exports = db;