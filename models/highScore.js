module.exports = (sequelize, Sequelize) => {
    const Users = sequelize.define('user', {
        user_name: {
            type: Sequelize.STRING
        },
        score: {
            type: Sequelize.INTEGER
        },
        email: {
            type: Sequelize.STRING
        }
    });

    return Users;
};